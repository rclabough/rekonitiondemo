import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import software.amazon.awssdk.auth.AwsCredentialsProvider;
import software.amazon.awssdk.auth.ProfileCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.rekognition.RekognitionClient;
import software.amazon.awssdk.services.rekognition.model.*;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.sync.RequestBody;

public class PhotoProcessor {

    private String _directory;
    private RekognitionClient _rekognition;
    private S3Client _s3Client;


    private Map<String, List<String>> _tagMap;
    private Map<String, List<String>> _personMap;
    private Map<String, List<String>> _faceMatches;

    public PhotoProcessor(String directory) {
        if (directory == null || "".equalsIgnoreCase(directory)) {
            throw new NullPointerException("directory may not be null");
        }
        _directory = directory;
        initializeAWS();
    }

    private void initializeAWS() {
        Region region = Region.US_WEST_2;

        AwsCredentialsProvider credentials = ProfileCredentialsProvider.builder()
                .profilesConfigFile(LocalConfiguration.LOCAL_AWS_CONFIG).build();
        _s3Client = S3Client.builder().region(region).credentialsProvider(credentials).build();
         _rekognition = RekognitionClient.builder().region(region).credentialsProvider(credentials).build();
         _tagMap = new HashMap<>();
         _personMap = new HashMap<>();
         _faceMatches = new HashMap<>();
    }

    public void process() {

        File directory = new File(_directory);
        File[] files = directory.listFiles();
        if (files == null || files.length == 0) {
            log("No files found");
            return;
        }
        UUID collectionId = UUID.randomUUID();
        CreateCollectionResponse collectionResponse = _rekognition.createCollection(CreateCollectionRequest.builder().collectionId(collectionId.toString()).build());


        for(File f : files) {
            try {
                processFile(f, collectionId);
            } catch (Exception ex) {
                log("Could not process file");
            }
        }

    }

    public Map<String, List<String>> getPeopleInPhotos() {
        return _personMap;
    }

    public Map<String, List<String>> getTaggedPhotos() {
        return _tagMap;
    }

    public Map<String, List<String>> getMatchedFaces() {
        return _faceMatches;

    }

    private void processFile(File file, UUID collectionId) throws Exception {


        String extension = FilenameUtils.getExtension(file.getName());

        UUID key = UUID.randomUUID();
        String fileName = buildFileName(key, extension);

        _s3Client.putObject(PutObjectRequest.builder().bucket(LocalConfiguration.S3_BUCKET).key(fileName).build(), RequestBody.of(file));



        List<String> tags = getTags(fileName);
        List<FaceRecord> faces = detectFaces(fileName, collectionId);

        log("----------------");
        log("For file " + file.getName() + " the following tags were found:");
        for(String s : tags) {
            log("\t" + s);
            addTagToMap(s, fileName);
        }
        log("Faces were found: ");
        for(FaceRecord fr : faces) {
            log("\t" + fr.face().faceId());
            addPersonToMap(fr.face().faceId(), fileName);
        }

        log ("Matching faces...");
        Set<String> faceIds = _personMap.keySet();

        for(String faceId : faceIds) {
            List<FaceMatch> matches = searchFaces(faceId, collectionId);
            for(FaceMatch fm : matches) {
                addFaceMatchToMap(faceId, fm.face().faceId());
            }
        }
    }

    private void addTagToMap(String tag, String file) {
        if (!_tagMap.containsKey(tag)) {
            _tagMap.put(tag, new ArrayList<>());
        }
        _tagMap.get(tag).add(file);
    }
    private void addPersonToMap(String person, String file) {
        if (!_personMap.containsKey(person)) {
            _personMap.put(person, new ArrayList<>());
        }
        _personMap.get(person).add(file);
    }

    private void addFaceMatchToMap(String faceId, String matchedId) {
        if (!_faceMatches.containsKey(faceId)) {
            _faceMatches.put(faceId, new ArrayList<>());
        }
        _faceMatches.get(faceId).add(matchedId);
    }

    private Image getImageByFileName(String fileName) {
        return Image.builder().s3Object(S3Object.builder().bucket(LocalConfiguration.S3_BUCKET).name(fileName).build()).build();
    }

    private List<FaceRecord> detectFaces(String fileName, UUID collectionId) {
        IndexFacesRequest indexRequest = IndexFacesRequest.builder().collectionId(collectionId.toString()).image(getImageByFileName(fileName)).build();
        try {

            IndexFacesResponse indexFacesResponse = _rekognition.indexFaces(indexRequest);
            return indexFacesResponse.faceRecords();

        } catch (Exception ex) {
            log(ex.getMessage());
            ex.printStackTrace();
        }
        return null;
    }


    private List<String> getTags(String file) {

        DetectLabelsRequest request = DetectLabelsRequest.builder()
                .image(getImageByFileName(file))
                //.minConfidence()
                .build();

        try {
            DetectLabelsResponse result = _rekognition.detectLabels(request);
            List <Label> labels = result.labels();

            return labels.stream().filter(k -> k.confidence() > 0.75f).map(Label::name).collect(Collectors.toList());
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private List<FaceMatch> searchFaces(String faceId, UUID collectionId) {
        SearchFacesRequest searchFacesRequest = SearchFacesRequest.builder()
                .collectionId(collectionId.toString())
                .faceId(faceId)
                .faceMatchThreshold(70F).build();

        try {
            SearchFacesResponse response = _rekognition.searchFaces(searchFacesRequest);
            return response.faceMatches();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private String buildFileName(UUID key, String extension) {
        return key.toString() + "." + extension;
    }

    private void log(String message) {
        System.out.println(message);
    }

}
