import software.amazon.awssdk.auth.profile.ProfilesConfigFile;

public class LocalConfiguration {

    public static final String S3_BUCKET = "bloomzrekognitiondemo";

    public static final String LOCAL_PHOTOS_DIRECTORY = "/home/rclabough/Pictures";
    public static final ProfilesConfigFile LOCAL_AWS_CONFIG = new ProfilesConfigFile("/home/rclabough/source/grafanacloud/docker-scripts/.aws-config");
}
