import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        String directory = LocalConfiguration.LOCAL_PHOTOS_DIRECTORY;

        PhotoProcessor processor = new PhotoProcessor(directory);

        processor.process();

        Map<String, List<String>> personRecommendations = processor.getPeopleInPhotos();

        prettyDisplay("People:", personRecommendations);

        Map<String, List<String>> tagRecommendations = processor.getTaggedPhotos();

        prettyDisplay("Tags:", tagRecommendations);

        Map<String, List<String>> matchedFaces = processor.getMatchedFaces();

        prettyDisplay("Matches:", matchedFaces);


    }

    private static void prettyDisplay(String title, Map<String, List<String>> map) {
        System.out.println("-----------------");
        System.out.println(title);

        for(String key : map.keySet()) {
            System.out.println(key + ":");
            for(String item : map.get(key)) {
                System.out.println("\t- " + item );
            }
        }
        System.out.println();
    }
}
